# Helm Chart

Helm chart template to use across services 


### To add this Helm Chart to your project run:

`git submodule add --name helm-chart ../helm-chart.git chart`



### Then  add the file `chart.yml` to your service root folder


```yml
ingress:
  hosts:
    - host: api.dev.youclap.tech # TODO change this to api.dev.youclap.tech when available
      paths:
        - /<service-name>
``` 
